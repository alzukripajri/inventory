-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Jun 2020 pada 10.44
-- Versi server: 10.1.30-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sitariangkot`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `id_barang` char(7) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `stok` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `jenis_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `stok`, `satuan_id`, `jenis_id`) VALUES
('B000001', 'mobil', 52, 24, 1),
('B000002', 'kain jelek', 21, 19, 7),
('B000003', 'coba coba', 0, 24, 1),
('B000004', 'kuda kudaan', 10, 24, 1),
('B000005', 'baju belang', 0, 24, 1),
('B000006', 'baju hijau', 0, 24, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `id_barang_keluar` char(16) NOT NULL,
  `barang_id` char(7) NOT NULL,
  `jumlah_keluar` int(11) NOT NULL,
  `tanggal_keluar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang_keluar`
--

INSERT INTO `barang_keluar` (`id_barang_keluar`, `barang_id`, `jumlah_keluar`, `tanggal_keluar`) VALUES
('T-BK-20052700001', 'B000002', 20, '2020-05-27'),
('T-BK-20060900001', 'B000002', 6, '2020-06-09'),
('T-BK-20061500001', 'B000004', 5, '2020-06-15');

--
-- Trigger `barang_keluar`
--
DELIMITER $$
CREATE TRIGGER `update_stok_keluar` BEFORE INSERT ON `barang_keluar` FOR EACH ROW UPDATE `barang` SET `barang`.`stok` = `barang`.`stok` - NEW.jumlah_keluar WHERE `barang`.`id_barang` = NEW.barang_id
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id_barang_masuk` char(16) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `barang_id` char(7) NOT NULL,
  `jumlah_masuk` int(11) NOT NULL,
  `tanggal_masuk` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `barang_masuk`
--

INSERT INTO `barang_masuk` (`id_barang_masuk`, `supplier_id`, `barang_id`, `jumlah_masuk`, `tanggal_masuk`) VALUES
('T-BM-20052700001', 1, 'B000002', 12, '2020-05-27'),
('T-BM-20052700002', 1, 'B000001', 13, '2020-05-27'),
('T-BM-20060900001', 1, 'B000001', 15, '2020-06-09'),
('T-BM-20061000001', 1, 'B000002', 15, '2020-06-10'),
('T-BM-20061400001', 1, 'B000001', 12, '2020-06-14'),
('T-BM-20061500001', 1, 'B000004', 15, '2020-06-15'),
('T-BM-20061600001', 2, 'B000001', 12, '2020-06-16');

--
-- Trigger `barang_masuk`
--
DELIMITER $$
CREATE TRIGGER `update_stok_masuk` BEFORE INSERT ON `barang_masuk` FOR EACH ROW UPDATE `barang` SET `barang`.`stok` = `barang`.`stok` + NEW.jumlah_masuk WHERE `barang`.`id_barang` = NEW.barang_id
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`) VALUES
(1, 'baju'),
(5, 'lolololo'),
(7, 'kain');

-- --------------------------------------------------------

--
-- Struktur dari tabel `req_barangkeluar`
--

CREATE TABLE `req_barangkeluar` (
  `id_req_barangkeluar` int(11) NOT NULL,
  `tgl_rbk` date NOT NULL,
  `barang_id` char(7) NOT NULL,
  `jumlah_pesananbk` int(11) NOT NULL,
  `statusbk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `req_barangkeluar`
--

INSERT INTO `req_barangkeluar` (`id_req_barangkeluar`, `tgl_rbk`, `barang_id`, `jumlah_pesananbk`, `statusbk_id`) VALUES
(3, '2020-06-17', 'B000004', 15, 2),
(4, '2020-06-17', 'B000001', 30, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `req_barangmasuk`
--

CREATE TABLE `req_barangmasuk` (
  `id_req_barangmasuk` int(11) NOT NULL,
  `tgl_rbm` date NOT NULL,
  `barang_id` char(7) NOT NULL,
  `jumlah_pesanan` int(11) NOT NULL,
  `statusbm_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `req_barangmasuk`
--

INSERT INTO `req_barangmasuk` (`id_req_barangmasuk`, `tgl_rbm`, `barang_id`, `jumlah_pesanan`, `statusbm_id`) VALUES
(6, '2020-06-17', 'B000004', 13, 1),
(7, '2020-06-16', 'B000001', 30, 1),
(8, '2020-06-17', 'B000001', 30, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `satuan`
--

CREATE TABLE `satuan` (
  `id_satuan` int(11) NOT NULL,
  `nama_satuan` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `satuan`
--

INSERT INTO `satuan` (`id_satuan`, `nama_satuan`) VALUES
(1, 'kodok'),
(12, 'kkk kkkk'),
(19, 'buah'),
(24, 'pack');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status`
--

CREATE TABLE `status` (
  `id_status` int(11) NOT NULL,
  `nama_status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `status`
--

INSERT INTO `status` (`id_status`, `nama_status`) VALUES
(1, 'Belum dipesan'),
(2, 'Sudah dipesan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statusbk`
--

CREATE TABLE `statusbk` (
  `id_statusbk` int(11) NOT NULL,
  `nama_statusbk` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `statusbk`
--

INSERT INTO `statusbk` (`id_statusbk`, `nama_statusbk`) VALUES
(1, 'Belum dikirim'),
(2, 'Sudah dikirim');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statusbm`
--

CREATE TABLE `statusbm` (
  `id_statusbm` int(11) NOT NULL,
  `nama_statusbm` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `statusbm`
--

INSERT INTO `statusbm` (`id_statusbm`, `nama_statusbm`) VALUES
(1, 'Belum dipesan'),
(2, 'Sudah dipesan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(50) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `no_rek` int(11) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `no_telp`, `no_rek`, `alamat`) VALUES
(1, 'bambang', '089893022', 0, 'jauh di mato dekat dihati'),
(2, 'julio', '0819231312', 2147483647, 'Dimana mana hatiku senang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `role` enum('admin','hrd','finance','purchase','produksi','sales','inventory') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `username`, `password`, `nama_user`, `role`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Pajri Al Zukri', 'admin'),
(5, 'inventory', '7a1eabc3deb7fd02ceb1e16eafc41073', 'Salman Affandi', 'inventory'),
(8, 'finance', '57336afd1f4b40dfd9f5731e35302fe5', 'Ellin', 'finance'),
(9, 'pajri', 'b2f08dc583b19bc9b5a9520d94b26632', 'Pajri Al Zukri', 'inventory');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `satuan_id` (`satuan_id`),
  ADD KEY `jenis_id` (`jenis_id`);

--
-- Indeks untuk tabel `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id_barang_keluar`),
  ADD KEY `barang_id` (`barang_id`);

--
-- Indeks untuk tabel `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id_barang_masuk`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `barang_id` (`barang_id`);

--
-- Indeks untuk tabel `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indeks untuk tabel `req_barangkeluar`
--
ALTER TABLE `req_barangkeluar`
  ADD PRIMARY KEY (`id_req_barangkeluar`),
  ADD KEY `barang_id` (`barang_id`),
  ADD KEY `statusbk_id` (`statusbk_id`);

--
-- Indeks untuk tabel `req_barangmasuk`
--
ALTER TABLE `req_barangmasuk`
  ADD PRIMARY KEY (`id_req_barangmasuk`),
  ADD KEY `barang_id` (`barang_id`),
  ADD KEY `statusbm_id` (`statusbm_id`) USING BTREE;

--
-- Indeks untuk tabel `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id_satuan`);

--
-- Indeks untuk tabel `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indeks untuk tabel `statusbk`
--
ALTER TABLE `statusbk`
  ADD PRIMARY KEY (`id_statusbk`);

--
-- Indeks untuk tabel `statusbm`
--
ALTER TABLE `statusbm`
  ADD PRIMARY KEY (`id_statusbm`);

--
-- Indeks untuk tabel `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `req_barangkeluar`
--
ALTER TABLE `req_barangkeluar`
  MODIFY `id_req_barangkeluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `req_barangmasuk`
--
ALTER TABLE `req_barangmasuk`
  MODIFY `id_req_barangmasuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id_satuan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `status`
--
ALTER TABLE `status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `statusbk`
--
ALTER TABLE `statusbk`
  MODIFY `id_statusbk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `statusbm`
--
ALTER TABLE `statusbm`
  MODIFY `id_statusbm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`satuan_id`) REFERENCES `satuan` (`id_satuan`),
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`jenis_id`) REFERENCES `jenis` (`id_jenis`);

--
-- Ketidakleluasaan untuk tabel `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD CONSTRAINT `barang_keluar_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id_barang`);

--
-- Ketidakleluasaan untuk tabel `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD CONSTRAINT `barang_masuk_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id_supplier`),
  ADD CONSTRAINT `barang_masuk_ibfk_2` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id_barang`);

--
-- Ketidakleluasaan untuk tabel `req_barangkeluar`
--
ALTER TABLE `req_barangkeluar`
  ADD CONSTRAINT `req_barangkeluar_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id_barang`),
  ADD CONSTRAINT `req_barangkeluar_ibfk_2` FOREIGN KEY (`statusbk_id`) REFERENCES `statusbk` (`id_statusbk`);

--
-- Ketidakleluasaan untuk tabel `req_barangmasuk`
--
ALTER TABLE `req_barangmasuk`
  ADD CONSTRAINT `req_barangmasuk_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id_barang`),
  ADD CONSTRAINT `req_barangmasuk_ibfk_2` FOREIGN KEY (`statusbm_id`) REFERENCES `statusbm` (`id_statusbm`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
