     <div class="main-panel">
<div class="content-wrapper">
           
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span>Request Barang Keluar </h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">


                	<!-- di comment dulu karena inven gak bisa input ini -->

                  <!-- <li class="breadcrumb-item active" aria-current="page">
                     <a href="<?php echo base_url('index.php/inventory/req_keluar/add') ?>"> <button class="btn btn-primary" >
                  <i class="mdi mdi-plus"></i>&nbsp; Tambah Request Barang Keluar</button></a>
                  </li> -->
                </ul>
              </nav>
            </div>
          
           
            <div class="row">
              <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Recent Tickets</h4> -->
                    <div class="table-responsive">
        <table id="dataTable" class="table table-striped table-boarder" width="100%" align="center">
            <thead>
                <tr>
                    <th>No. </th>
                      <th>Tanggal</th>
                    <th>Nama Barang</th>
                    <th>Jumlah Permintaan</th>
                    <th>Status Barang</th>

                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($req_barangkeluar) :
                    foreach ($req_barangkeluar as $bk) :
                        ?>
                        <tr>
                            <td><?= $no++; ?></td>
                             <td><?= $bk['tgl_rbk']; ?></td>
                            <td><?= $bk['nama_barang']; ?></td>
                            <td><?= $bk['jumlah_pesananbk']; ?></td>
                             <td><?= $bk['nama_statusbk']; ?></td>
                           
                            <td>
                            	<a href="<?= base_url('index.php/inventory/req_keluar/edit/') . $bk['id_req_barangkeluar'] ?>" class="btn btn-warning btn-circle btn-sm"><i class="mdi mdi-pencil"></i></a>
                                <a onclick="return confirm('Yakin ingin hapus?')" href="<?php echo base_url('index.php/inventory/req_keluar/delete/') . $bk['id_req_barangkeluar'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="8" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>


    
      





                  </div>
                </div>
              </div>
            </div>
            
        		</div>

    