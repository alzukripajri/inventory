     <div class="main-panel">
<div class="content-wrapper">
           
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span> Satuan Barang </h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <!-- <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i> -->
                  </li>
                   <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >
                  <i class="mdi mdi-plus"></i>&nbsp; Tambah Satuan Barang</button>
                </ul>
              </nav>
            </div>
           <?= $this->session->flashdata('pesan'); ?>
           
            <div class="row">
              <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Recent Tickets</h4> -->
                   
        <div class="table-responsive">
        <table id="dataTable" class="table table-striped table-boarder" width="100%" align="center">
            <thead>
                <tr>
                    <th>No. </th>
                    <th>Nama Satuan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($satuan) :
                    foreach ($satuan as $j) :
                        ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $j['nama_satuan']; ?></td>
                            <td>
                                <a href="<?php echo base_url('index.php/inventory/satuan/edit/') . $j['id_satuan'] ?>"  class="btn btn-warning btn-circle btn-sm" ><i class="mdi mdi-pencil"></i></a> 
                             
                                <a onclick="return confirm('Yakin ingin hapus?')" href="<?php echo base_url('index.php/inventory/satuan/delete/') . $j['id_satuan'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="3" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>


 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Satuan Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="background-color: white ">
      <Form method="POST"  action="<?php echo site_url('inventory/satuan/add')?>">
         <div class="form group">
          <input type="hidden" name="id_satuan" id="id_satuan">
           <label style="margin-bottom: 20px; margin-top: 20px">Satuan Barang</label>
           <input value="<?= set_value('nama_satuan'); ?>"  name="nama_satuan" id="nama_satuan" type="text" class="form-control" placeholder="Nama Satuan...">
                       
         </div>


        <div style="margin-top: 30px; float: right">
         <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" >Simpan</button>
        
        </div>
        </Form>    
              
              </div>
            </div>
           
          </div>
       </div>






                  </div>
                </div>
              </div>
            </div>
            
            </div>

    