     <div class="main-panel">
<div class="content-wrapper">
           
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span>Request Barang Masuk </h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                     <a href="<?php echo base_url('index.php/inventory/req_masuk/add') ?>"> <button class="btn btn-primary" >
                  <i class="mdi mdi-plus"></i>&nbsp; Tambah Request Barang Masuk</button></a>
                  </li>
                </ul>
              </nav>
            </div>
          
           
            <div class="row">
              <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Recent Tickets</h4> -->
                    <div class="table-responsive">
        <table id="dataTable" class="table table-striped table-boarder" width="100%" align="center">
            <thead>
                <tr>
                    <th>No. </th>
                    <th>Tanggal</th>
                    <th>Nama Barang</th>
                    <th>Jumlah Pesanan</th>
                    <th>Status Barang</th>

                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($req_barangmasuk) :
                    foreach ($req_barangmasuk as $bm) :
                        ?>
                        <tr>
                            <td><?= $no++; ?></td>
                             <td><?= $bm['tgl_rbm']; ?></td>
                            <td><?= $bm['nama_barang']; ?></td>
                            <td><?= $bm['jumlah_pesanan']; ?></td>
                             <td><?= $bm['nama_statusbm']; ?></td>
                           
                            <td>
                            	<a href="<?= base_url('index.php/inventory/req_masuk/edit/') . $bm['id_req_barangmasuk'] ?>" class="btn btn-warning btn-circle btn-sm"><i class="mdi mdi-pencil"></i></a>
                                <a onclick="return confirm('Yakin ingin hapus?')" href="<?php echo base_url('index.php/inventory/req_masuk/delete/') . $bm['id_req_barangmasuk'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="8" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>


    
      





                  </div>
                </div>
              </div>
            </div>
            
        		</div>

    