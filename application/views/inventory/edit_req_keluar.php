     <div class="main-panel">
<div class="content-wrapper">
           
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span> Edit Request Barang Keluar </h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <!-- <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i> -->
                  </li>
                  
                </ul>
              </nav>
            </div>
          
           
            <div class="row">
              <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Recent Tickets</h4> -->
                
                 <?= $this->session->flashdata('pesan'); ?>
                <?= form_open('', [], ['id_req_barangkeluar' => $req_barangkeluar['id_req_barangkeluar']]);?>

                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="tgl_rbk" style="margin-top: 15px">Tanggal Request</label>
                    <div class="col-md-9">
                        <input value="<?= set_value('tgl_rbk', $req_barangkeluar['tgl_rbk']); ?>" name="tgl_rbk" id="tgl_rbk" type="date" class="form-control" placeholder="Tanggal Request..." >
                        <?= form_error('tgl_rbk', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="barang_id">Nama Barang</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <select name="barang_id" id="barang_id" class="custom-select" disabled ="true">
                                <option value="" selected disabled>Pilih Barang</option>
                                <?php foreach ($barang as $b) : ?>
                                    <option <?= $req_barangkeluar['barang_id'] == $b['id_barang'] ? 'selected' : ''; ?> <?= set_select('barang_id', $b['id_barang']) ?> value="<?= $b['id_barang'] ?>"><?= $b['nama_barang'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <input value="<?= set_value('barang_id', $req_barangkeluar['barang_id']); ?>" name="barang_id" id="barang_id" type="hidden" class="form-control" placeholder="Jumlah Permintaan...">
                        </div>
                        <?= form_error('barang_id', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>

                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="jumlah_pesananbk" style="margin-top: 15px">Jumlah Permintaan</label>
                    <div class="col-md-9">
                        <input value="<?= set_value('jumlah_pesananbk', $req_barangkeluar['jumlah_pesananbk']); ?>" name="jumlah_pesananbk" id="jumlah_pesananbk" type="text" class="form-control" placeholder="Jumlah Permintaan..." readonly>
                        <?= form_error('jumlah_pesananbk', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>

                 <!-- khusus untuk inventory status barang tidak dapat di edit -->

                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="statusbk_id">Status Barang</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <select name="statusbk_id" id="statusbk_id" class="custom-select" >
                                <option value="" selected disabled >Pilih Status</option>
                                <?php foreach ($statusbk as $s) : ?>
                                    <option <?= $req_barangkeluar['statusbk_id'] == $s['id_statusbk'] ? 'selected' : ''; ?> <?= set_select('statusbk_id', $s['id_statusbk']) ?> value="<?= $s['id_statusbk'] ?>"><?= $s['nama_statusbk'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            
                        </div>
                        <?= form_error('statusbk_id', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div style="margin-top: 6px; float: right">
                    
                 <button type="submit" class="btn btn-primary" >Simpan</button>
        
                </div>
                <?= form_close(); ?>
        
                 
                <div  style="margin-top: 30px;">
                        <a href="<?= base_url('index.php/inventory/req_keluar') ?>" class="btn btn-secondary">
                           
                                Kembali
                           
                        </a>
                    </div>




                  </div>
                </div>
              </div>
            </div>
            
            </div>

    