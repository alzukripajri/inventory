     <div class="main-panel">
<div class="content-wrapper">
           
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span> Jenis Barang </h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >
                  <i class="mdi mdi-plus"></i>&nbsp; Tambah Jenis Barang</button>
                  </li>
                </ul>
              </nav>
            </div>
           <?= $this->session->flashdata('pesan'); ?>
            
            <div class="row">
              <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                 
                   <div class="table-responsive">
        <table id="dataTable" class="table table-striped table-boarder" width="100%" align="center">

            <thead>
                <tr>
                    <th>No. </th>
                    <th>Nama Jenis</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                if ($jenis) :
                    foreach ($jenis as $j) :
                        ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $j['nama_jenis']; ?></td>
                            <td>
                                <a href="<?php echo base_url('index.php/inventory/jenis/edit/') . $j['id_jenis'] ?>" class="btn btn-warning btn-circle btn-sm"><i class="mdi mdi-pencil"></i></a>
                                <a onclick="return confirm('Yakin ingin hapus?')" href="<?php echo base_url('index.php/inventory/jenis/delete/') . $j['id_jenis'] ?>" class="btn btn-danger btn-circle btn-sm"><i class="mdi mdi-delete"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="3" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Jenis Barang</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="background-color: white ">
      <Form method="POST"  action="<?php echo site_url('inventory/jenis/add')?>">
         <div class="form group">
          <input type="hidden" name="id_jenis" id="id_jenis">
           <label style="margin-bottom: 20px; margin-top: 20px">Jenis Barang</label>
           <input value="<?= set_value('nama_jenis'); ?>" name="nama_jenis" id="nama_jenis" type="text" class="form-control" placeholder="Nama Jenis...">
                        <?= form_error('nama_jenis', '<small class="text-danger">', '</small>'); ?>
                       
         </div>


        <div style="margin-top: 30px; float: right">
         <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" >Simpan</button>
        
        </div>
        </Form>    
              
              </div>
            </div>
           
          </div>
       </div>



                  </div>
                </div>
              </div>
            </div>
            
        		</div>

    