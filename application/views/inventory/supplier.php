     <div class="main-panel">
<div class="content-wrapper">
           
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span> Supplier </h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <!-- <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i> -->
                  </li>
                   <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >
                  <i class="mdi mdi-plus"></i>&nbsp; Tambah Supplier</button>
                </ul>
              </nav>
            </div>
           <?= $this->session->flashdata('pesan'); ?>
           
            <div class="row">
              <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Recent Tickets</h4> -->
                   
        <div class="table-responsive">
        <table id="dataTable" class="table table-striped table-boarder" width="100%" align="center">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Nomor Telepon</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($supplier) :
                    $no = 1;
                    foreach ($supplier as $s) :
                        ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $s['nama_supplier']; ?></td>
                            <td><?= $s['no_telp']; ?></td>
                            <td><?= $s['alamat']; ?></td>
                            <th>
                                <a href="<?php echo base_url('index.php/inventory/supplier/edit/') . $s['id_supplier'] ?>" class="btn btn-circle btn-warning btn-sm"><i class="mdi mdi-pencil"></i></a>
                                <a onclick="return confirm('Yakin ingin hapus?')" href="<?php echo base_url('index.php/inventory/supplier/delete/') . $s['id_supplier'] ?>" class="btn btn-circle btn-danger btn-sm"><i class="mdi mdi-delete"></i></a>
                            </th>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="6" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>


 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Supplier</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="background-color: white ">
      <Form method="POST"  action="<?php echo site_url('inventory/supplier/add')?>">
         <div class="form group">
          <input type="hidden" name="id_supplier" id="id_supplier">
           <label style="margin-bottom: 20px; margin-top: 20px">Nama Suppier</label>
           <input value="<?= set_value('nama_supplier'); ?>" name="nama_supplier" id="nama_supplier" type="text" class="form-control" placeholder="Nama Supplier...">     
         </div>

          <div class="form group">
          
           <label style="margin-bottom: 20px; margin-top: 20px">No Telepon</label>
           <input value="<?= set_value('no_telp'); ?>" name="no_telp" id="no_telp" type="number" class="form-control" placeholder="Nomor Telepon...">   
         </div>


           <div class="form group">
          
           <label style="margin-bottom: 20px; margin-top: 20px">No Rekening</label>
           <input value="<?= set_value('no_rek'); ?>" name="no_rek" id="no_rek" type="number" class="form-control" placeholder="Nomor Rekening...">   
         </div>

          <div class="form group">
          
           <label style="margin-bottom: 20px; margin-top: 20px">Alamat</label>
            <textarea name="alamat" id="alamat" class="form-control" rows="4" placeholder="Alamat..."><?= set_value('alamat'); ?></textarea>    
         </div>

        


        <div style="margin-top: 30px; float: right">
         <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" >Simpan</button>
        
        </div>
        </Form>    
              
              </div>
            </div>
           
          </div>
       </div>






                  </div>
                </div>
              </div>
            </div>
            
            </div>

    