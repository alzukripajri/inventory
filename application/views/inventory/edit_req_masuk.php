     <div class="main-panel">
<div class="content-wrapper">
           
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span> Edit Request Barang Masuk </h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <!-- <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i> -->
                  </li>
                  
                </ul>
              </nav>
            </div>
          
           
            <div class="row">
              <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Recent Tickets</h4> -->
                
                 <?= $this->session->flashdata('pesan'); ?>
                <?= form_open('', [], ['id_req_barangmasuk' => $req_barangmasuk['id_req_barangmasuk']]);?>
                
                
                  <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="tgl_rbm" style="margin-top: 15px">Tanggal Request</label>
                    <div class="col-md-9">
                        <input value="<?= set_value('tgl_rbm', $req_barangmasuk['tgl_rbm']); ?>" name="tgl_rbm" id="tgl_rbm" type="date" class="form-control" placeholder="Tanggal Request..." >
                        <?= form_error('tgl_rbm', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>

                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="barang_id">Nama Barang</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <select name="barang_id" id="barang_id" class="custom-select">
                                <option value="" selected disabled>Pilih Barang</option>
                                <?php foreach ($barang as $b) : ?>
                                    <option <?= $req_barangmasuk['barang_id'] == $b['id_barang'] ? 'selected' : ''; ?> <?= set_select('barang_id', $b['id_barang']) ?> value="<?= $b['id_barang'] ?>"><?= $b['nama_barang'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <!-- 
                            <input value="<?= set_value('barang_id', $req_barangmasuk['barang_id']); ?>"<?= $b['nama_barang'] ?> name="barang_id" id="barang_id" type="text" class="form-control" placeholder="Jumlah Pesanan..." readonly>
                       		 -->
                        <?= form_error('barang_id', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>

                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="jumlah_pesanan" style="margin-top: 15px">Jumlah Pesanan</label>
                    <div class="col-md-9">
                        <input value="<?= set_value('jumlah_pesanan', $req_barangmasuk['jumlah_pesanan']); ?>" name="jumlah_pesanan" id="jumlah_pesanan" type="text" class="form-control" placeholder="Jumlah Pesanan..." >
                        <?= form_error('jumlah_pesanan', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>

                 <!-- khusus untuk inventory status barang tidak dapat di edit -->

                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="statusbm_id">Status Barang</label>
                    <div class="col-md-9">
                        <div class="input-group" >
                            <select name="statusbm_id" id="statusbm_id" class="custom-select" disabled="true">
                                <option value="" selected disabled >Pilih Status</option>
                                <?php foreach ($statusbm as $s) : ?>
                                    <option <?= $req_barangmasuk['statusbm_id'] == $s['id_statusbm'] ? 'selected' : ''; ?> <?= set_select('statusbm_id', $s['id_statusbm']) ?> value="<?= $s['id_statusbm'] ?>"><?= $s['nama_statusbm'] ?></option>
                                <?php endforeach; ?>
                            </select>
                             <input value="<?= set_value('statusbm_id', $req_barangmasuk['statusbm_id']); ?>" name="statusbm_id" id="statusbm_id" type="hidden" class="form-control" placeholder="Jumlah Pesanan..." readonly>
                        </div>
                        <?= form_error('statusbm_id', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div style="margin-top: 6px; float: right">
                    
                 <button type="submit" class="btn btn-primary" >Simpan</button>
        
                </div>
                <?= form_close(); ?>
        
                 
                <div  style="margin-top: 30px;">
                        <a href="<?= base_url('index.php/inventory/req_masuk') ?>" class="btn btn-secondary">
                           
                                Kembali
                           
                        </a>
                    </div>




                  </div>
                </div>
              </div>
            </div>
            
            </div>

    