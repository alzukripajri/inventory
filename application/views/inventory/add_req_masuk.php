     <div class="main-panel">
<div class="content-wrapper">
           
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span> Tambah Request Barang Masuk </h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <!-- <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i> -->
                  </li>
                  
                </ul>
              </nav>
            </div>
          
           
            <div class="row">
              <div class="col-12 grid-margin">
                <div class="card">
                  <div class="card-body">
                    <!-- <h4 class="card-title">Recent Tickets</h4> -->
                
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open(); ?>
               
                   
                        <input type="hidden" name="id_req_barangmasuk" id="id_req_barangmasuk">
            
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="tgl_rbm">Tanggal Request</label>
                    <div class="col-md-4">
                        <input value="<?= set_value('tgl_rbm'); ?>" name="tgl_rbm" id="tgl_rbm" type="date" class="form-control" placeholder="Tanggal Masuk...">
                        <?= form_error('tgl_rbm', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="barang_id">Barang</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <select name="barang_id" id="barang_id" class="custom-select">
                                <option value="" selected disabled>Pilih Barang</option>
                                <?php foreach ($barang as $b) : ?>
                                    <option <?= $this->uri->segment(3) == $b['id_barang'] ? 'selected' : '';  ?> <?= set_select('barang_id', $b['id_barang']) ?> value="<?= $b['id_barang'] ?>"><?= $b['id_barang'] . ' | ' . $b['nama_barang'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            
                        </div>
                        <?= form_error('barang_id', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>

                <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="jumlah_pesanan">Jumlah Pesanan</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <input value="<?= set_value('jumlah_pesanan'); ?>" name="jumlah_pesanan" id="jumlah_pesanan" type="number" class="form-control" placeholder="Jumlah Pesanan...">
                           
                        </div>
                        <?= form_error('jumlah_pesanan', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>

                 <div class="row form-group">
                    <label class="col-md-4 text-md-right" for="statusbm_id">Status Barang</label>
                    <div class="col-md-5">
                        <div class="input-group">
                            <select name="statusbm_id" id="statusbm_id" class="custom-select">
                                <option value="" selected disabled>Pilih Status</option>
                                <?php foreach ($statusbm as $s) : ?>
                                    <option <?= set_select('statusbm_id', $s['id_statusbm']) ?> value="<?= $s['id_statusbm'] ?>"><?= $s['nama_statusbm'] ?></option>
                                <?php endforeach; ?>
                            </select>
                           
                        </div> 
                        <?= form_error('statusbm_id', '<small class="text-danger">', '</small>'); ?>
                    </div>
                </div>
               
                
                
                <div style="margin-top: 6px; float: right">
        		 	
       			 <button type="submit" class="btn btn-primary" >Simpan</button>
        
        		</div>
                <?= form_close(); ?>
        
                 
                <div  style="margin-top: 30px;">
                        <a href="<?= base_url('index.php/inventory/req_masuk') ?>" class="btn btn-secondary">
                           
                                Kembali
                           
                        </a>
                    </div>




                  </div>
                </div>
              </div>
            </div>
            
            </div>

    