<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Sistem Enterprise</title>


	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
	<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo  base_url('asset/') ?>images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>vendor/bootstrap/css/bootstrap.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>fonts/iconic/css/material-design-iconic-font.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>vendor/animate/animate.css"/>
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>vendor/css-hamburgers/hamburgers.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>vendor/animsition/css/animsition.min.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>vendor/select2/select2.min.css"/>
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>vendor/daterangepicker/daterangepicker.css"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>css/util.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/') ?>css/main.css"/>
</head>
<body>

	<div class="container-login100" style="background-image: url('<?php echo base_url(); ?>asset/images/bg-01.jpg')">
		<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
			

			<form class="login100-form validate-form" method="POST" action="<?php echo base_url(); ?>index.php/login">
				
				<span class="login100-form-title p-b-20">
					<img src="<?php echo  base_url('asset/') ?>images/logo1.jpeg" style="height: 200px; width: 200px">
					<!-- Sistem Enterprise <br> SI 6A -->
				</span>
					<?php if(isset($error)) { echo $error; }; ?>
				<div class="wrap-input100 validate-input m-b-20" data-validate="Username">
					<input class="input100" type="text" name="username" placeholder="Username">
					<?php echo form_error('username'); ?>
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input m-b-25" data-validate = "Password">
					<input class="input100" type="password" name="password" placeholder="Password">
					<?php echo form_error('password'); ?>
					<span class="focus-input100"></span>
				</div>

				<div class="container-login100-form-btn">
					<button class="login100-form-btn">
						Login
					</button>
				</div>

				
			</form>

			<!--   <form class="form-signin" method="POST" action="<?php  base_url(); ?>index.php/login">
                <?php if(isset($error)) { echo $error; }; ?>
                <div class="form-group wrap-input100">
                    <input class="inputform" type="text" class="form-control" name="username" placeholder="Masukkan Username Anda" >
                    <?php echo form_error('username'); ?>
                    <span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-user"></i>
                        </span>
                </div>
                <div class="form-group wrap-input100">
                    <input class="inputform" type="password" name="password" class="form-control" placeholder="Masukkan Password Anda">
                    <?php echo form_error('password'); ?>
                    <span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock"></i>
                        </span>
                </div>

                <button class="login100-form-btn" class="btn btn-lg btn-primary btn-block" name="btn-login" id="btn-login" type="submit">
                    Masuk</button>
    

                </form>
 -->
			
		</div>
		<div id="error" style="margin-top: 10px"></div>
	</div>
	
	
<!--===============================================================================================-->
	<script src="<?php echo base_url('asset/') ?>vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('asset/') ?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('asset/') ?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url('asset/') ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('asset/') ?>vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('asset/') ?>vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url('asset/') ?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('asset/') ?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('asset/') ?>js/main.js"></script>
</body>
</html>