     <div class="main-panel">
<div class="content-wrapper">
           
            <div class="page-header">
              <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi mdi-home"></i>
                </span> Kelola User </h3>
              <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                  <li class="breadcrumb-item active" aria-current="page">
                    <!-- <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i> -->

                  </li>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" >
                  <i class="fa fa-plus "></i>&nbsp;Tambah Data User</button>
                </ul>
              </nav>
            </div>
           <!--  <div class="container-fluid mt--7"> -->
      
      <div class="row mt-5">
        <div class="col mb-5 mb-xl-0">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col" style="margin-left : 50px">
                  <!-- <h3 class="mb-0">Kelola User</h3> -->
                </div>
             
                <table class="table" style="margin: 50px">
                <thead>
    <tr>
    <th scope="col">No</th>
      <th scope="col">Username</th>
      <th scope="col">Nama User</th>
      <th scope="col">Role</th>
      <th scope="col=">Aksi</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
   
  <?php 
  $no = 1;
  foreach ($user as $usr) :
  ?>

      
  <tr>
      <td><?php echo $no++ ?></td>
      <td><?php echo $usr->username ?></td>
      <td><?php echo $usr->nama_user ?></td>
      
      <td><?php echo $usr->role ?></td>
      <td style="cursor: pointer;" onClick="select_data(
        '<?=$usr->id_user ?>',
        '<?=$usr->username ?>',
        '<?=$usr->password ?>',
        '<?=$usr->nama_user ?>',
        '<?=$usr->role ?>'
      )" data-toggle="modal" data-target="#exampleModal"><i class="mdi mdi-pencil"></i> </td>
      <td>
       <a href="<?php echo base_url("index.php/admin/Dashboard/hapus/".$usr->id_user)?>"><i class="mdi mdi-delete" style="color: black"></i></a> 
      
      </td>
     
      
    </tr>
    <?php endforeach ;?>
   
  </tbody>
</table>
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="background-color: white ">
      <Form method="POST"  action="<?php echo site_url('admin/Dashboard/tambah_user')?>">
         <div class="form group">
          <input type="hidden" name="id_user" id="id_user">
           <label style="margin-bottom: 20px; margin-top: 20px">Username</label>
           <input type="text" name="username" class="form-control" id="username">
         </div>

         <div class="form group">
           <label style="margin-bottom: 20px; margin-top: 20px">Password</label>
           <input type="password" name="password" class="form-control" id="password">
         </div>

         <div class="form group">
           <label style="margin-bottom: 20px; margin-top: 20px">Nama User</label>
           <input type="text" name="nama_user" class="form-control" id="nama_user">
         </div>

         <div class="form-group">
          <label style="margin-bottom: 20px; margin-top: 20px">Pilih Role</label>
          <select id="role" name="role" class="form-control" id="role">
            <option value="">Pilih</option>
            <option value="admin">Admin</option>
            <option value="hrd">hrd</option>
            <option value="finance">finance</option>
            <option value="purchase">purchase</option>
            <option value="produksi">produksi</option>
            <option value="sales">sales</option>
            <option value="inventory">inventory</option>
          </select>
           
         </div>



        <div style="margin-top: 30px; float: right">
         <button type="reset" class="btn btn-danger" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" name="tambah_transaksi">Simpan</button>
        
        </div>
        </Form>    
              
              </div>
            </div>
           
          </div>
        </div>
      </div>



            
        		</div>
