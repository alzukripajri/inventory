<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Inventory</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo  base_url('asset/') ?>vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?php echo  base_url('asset/') ?>vendors/css/vendor.bundle.base.css">
    <link href="<?php echo base_url('asset/'); ?>vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="<?php echo base_url('asset/'); ?>vendor/datatables/buttons/css/buttons.bootstrap4.min.css" rel="stylesheet">
    <link href="<?php echo base_url('asset/'); ?>vendor/datatables/responsive/css/responsive.bootstrap4.min.css" rel="stylesheet">

     <link href="<?php echo base_url('asset/'); ?>vendor/daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="<?php echo base_url('asset/'); ?>vendor/gijgo/css/gijgo.min.css" rel="stylesheet">

    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="<?php echo  base_url('asset/') ?>css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="<?php echo  base_url('asset/') ?>images/logo1.jpeg" />
     <script src="https://kit.fontawesome.com/c5dfac8b9a.js" crossorigin="anonymous"></script>
     <script>
// function select_data($id_satuan,$nama_satuan){
// $("#id_satuan").val($id_satuan);
// $("#nama_satuan").val($nama_satuan);

// }

// function refresh(){
//     $("#id_satuan").val("");
// $("#nama_satuan").val("");


// }
</script>
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
          <a class="navbar-brand brand-logo" href="index.html"><img style="height: 50px; width: 180px" src="<?php echo  base_url('asset/') ?>images/logo2.jpeg" alt="logo" /></a>
          <a class="navbar-brand brand-logo-mini" href="index.html"><img src="<?php echo  base_url('asset/') ?>images/logo-mini.svg" alt="logo" /></a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-stretch">
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-menu"></span>
          </button>
         

          <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item nav-profile dropdown">
              <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <div class="nav-profile-img">
                  <img src="<?php echo  base_url('asset/') ?>images/faces/users.png" alt="image">
                  <span class="availability-status online"></span>
                </div>
                <div class="nav-profile-text" style="padding-right:  20px">
                  <!-- <p class="mb-1 text-black">Pajri Al Zukri</p> -->
                   <span class="mb-0 text-sm  font-weight-bold"><?php echo $this->session->userdata("user_nama") ?></span>
                   

                </div>
               <!-- <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i> -->
              </a>
              <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
               <!--  <a class="dropdown-item" href="#">
                  <i class="mdi mdi-cached mr-2 text-success"></i> Activity Log </a> -->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="<?= base_url() ?>index.php/login/logout">
                  <i class="mdi mdi-logout mr-2 text-primary"></i> Signout </a>
              </div>
            </li>


          </ul>
          

          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="nav-profile-image">

                  <img src="<?php echo  base_url('asset/') ?>images/faces/users.png" alt="profile">
                  <span class="login-status online"></span>
                  <!--change to offline or busy as needed-->
                </div>
                <div class="nav-profile-text d-flex flex-column" style="padding-right:  20px">
                  <!-- <span class="font-weight-bold mb-2">Pajri Al Zukri</span> -->
                   <span class="mb-0 text-sm  font-weight-bold"><?php echo $this->session->userdata("user_nama") ?></span><br>
                  <span class="text-secondary text-small">Staff Inventory</span>
                </div>
                <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= site_url('inventory/dashboard') ?>">
                <span class="menu-title">Dashboard</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" href="<?= site_url('inventory/supplier') ?>">
                <span class="menu-title">Supplier</span>
                <i class="mdi mdi-account-multiple menu-icon"></i>
              </a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link" href="<?= site_url('inventory/satuan') ?>">
                <span class="menu-title">Satuan Barang</span>
                <i class="mdi mdi-package-variant menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= site_url('inventory/jenis') ?>">
                <span class="menu-title">Jenis Barang</span>
                <i class="mdi mdi-package-variant menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= site_url('inventory/barang') ?>">
                <span class="menu-title">Barang</span>
                <i class="mdi mdi-package menu-icon"></i>
              </a>
            </li>
          
            <li class="nav-item">
              <a class="nav-link" href="<?= site_url('inventory/stock_in') ?>">
                <span class="menu-title">Barang Masuk</span>
                <i class="mdi mdi-package-down menu-icon"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= site_url('inventory/stock_out') ?>">
                <span class="menu-title">Barang Keluar</span>
                <i class="mdi mdi-package-up menu-icon"></i>
              </a>
            </li>
           <li class="nav-item">
              <a class="nav-link" href="<?= site_url('inventory/req_masuk') ?>">
                <span class="menu-title">Request Barang Masuk</span>
                <i class="mdi mdi-plus-box menu-icon"></i>
              </a>
            </li>
             <li class="nav-item">
              <a class="nav-link" href="<?= site_url('inventory/req_keluar') ?>">
                <span class="menu-title">Request Barang Keluar</span>
                <i class="mdi mdi-minus-box menu-icon"></i>
              </a>
            </li>
           <li class="nav-item">
              <a class="nav-link" href="<?= site_url('inventory/laporan_inven') ?>">
                <span class="menu-title">Laporan</span>
                <i class="mdi mdi-zip-box menu-icon"></i>
              </a>
            </li>

          </ul>
        </nav>