  <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between" >
              <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020 Sistem Enterprise 6A.</span>
              
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?php echo  base_url('asset/') ?>vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="<?php echo  base_url('asset/') ?>vendors/chart.js/Chart.min.js"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="<?php echo  base_url('asset/') ?>js/off-canvas.js"></script>
    <script src="<?php echo  base_url('asset/') ?>js/hoverable-collapse.js"></script>
    <script src="<?php echo  base_url('asset/') ?>js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="<?php echo  base_url('asset/') ?>js/dashboard.js"></script>
    <script src="<?php echo  base_url('asset/') ?>js/todolist.js"></script>
    <!-- End custom js for this page -->
  </body>
</html>