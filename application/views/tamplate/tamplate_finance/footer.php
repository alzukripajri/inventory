  <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between" >
              <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020 Sistem Enterprise 6A.</span>
              
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->


    <script src="<?php echo base_url('asset/') ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo  base_url('asset/') ?>vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="<?php echo  base_url('asset/') ?>vendors/chart.js/Chart.min.js"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="<?php echo  base_url('asset/') ?>js/off-canvas.js"></script>
    <script src="<?php echo  base_url('asset/') ?>js/hoverable-collapse.js"></script>
    <script src="<?php echo  base_url('asset/') ?>js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="<?php echo  base_url('asset/') ?>js/dashboard.js"></script>
    <script src="<?php echo  base_url('asset/') ?>js/todolist.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/datatables/buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/datatables/buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/datatables/jszip/jszip.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/datatables/pdfmake/pdfmake.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/datatables/pdfmake/vfs_fonts.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/datatables/buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/datatables/buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/datatables/buttons/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/datatables/responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url('asset/') ?>vendor/datatables/responsive/js/responsive.bootstrap4.min.js"></script>



     <script type="text/javascript">
        $(function() {
            $('.date').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd'
            });

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#tangal').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
            }

            $('#tanggal').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
                    'Hari ini': [moment(), moment()],
                    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 hari terakhir': [moment().subtract(6, 'days'), moment()],
                    '30 hari terakhir': [moment().subtract(29, 'days'), moment()],
                    'Bulan ini': [moment().startOf('month'), moment().endOf('month')],
                    'Bulan lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Tahun ini': [moment().startOf('year'), moment().endOf('year')],
                    'Tahun lalu': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
                }
            }, cb);

            cb(start, end);
        });

        $(document).ready(function() {
            var table = $('#dataTable').DataTable({
               
            
                lengthMenu: [
                    [5, 10, 25, 50, 100, -1],
                    [5, 10, 25, 50, 100, "All"]
                ],
                columnDefs: [{
                    targets: -1,
                    orderable: false,
                    searchable: false
                }]
            });

            table.buttons().container().appendTo('#dataTable_wrapper .col-md-5:eq(0)');
        });
    </script>
    <!-- End custom js for this page -->
  </body>
</html>