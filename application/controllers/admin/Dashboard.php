<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
         $this->load->model("User_model");
        //cek session dan level user
        if($this->admin->is_role() != "admin"){
            redirect("login/");
        }
    }

    public function index()
    {
          $data["user"] = $this->User_model->tampil_data()->result();
            
   $this->load->view("tamplate/tamplate_admin/header.php");
    $this->load->view("admin/test.php", $data);   
   $this->load->view("tamplate/tamplate_admin/footer.php");
            

    }
public function tambah_user()
{

    $this->User_model->tambah_user();
    redirect('admin/dashboard');
}

public function edit_user() {
    if($this->input->post('submit'))
    {
        if($this->User_model->validation('update'))
        {
            $this->User_model->edit();
            redirect('admin/dashboard');
        }
        $data['user'] = $this->User_model->view_edit($transaksi_id);
        
        $this->load->view("template_admin/header.php");
        $this->load->view("admin/user_edit.php", $data);
        $this->load->view("template_admin/footer.php");
    }
}
public function hapus ($id_user){
    $this->User_model->delete($id_user);
    redirect('admin/dashboard');
    
}

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}