<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satuan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model Inventory_model
        $this->load->model('Admin',  'admin');
        $this->load->model('Inventory_model');
        $this->load->library('form_validation');
         $this->load->helper(['url','form', 'fungsi_helper']);
        //cek session dan level user
        if($this->Admin->is_role() != "inventory"){
            redirect("login/");
        }
    }

    public function index()
    {
    	 $data['title'] = "Satuan";
        $data['satuan'] = $this->Inventory_model->get('satuan');
            
    $this->load->view("tamplate/tamplate_inventory/header.php");
    $this->load->view("inventory/satuan.php", $data);   
     $this->load->view("tamplate/tamplate_inventory/footer.php");
   
            

    }

     private function _validasi()
    {
        $this->form_validation->set_rules('nama_satuan', 'Nama Satuan', 'required|trim');
    }

    public function add()
    {
        $this->_validasi();

        if ($this->form_validation->run() == false) {
            $data['title'] = "Satuan";
             $this->load->view("inventory/satuan.php", $data);  
        } else {
            $input = $this->input->post(null, true);
            $insert = $this->Inventory_model->insert('satuan', $input);
            if ($insert) {
                set_pesan('data berhasil disimpan');
                redirect('inventory/satuan');
            } else {
                set_pesan('data gagal disimpan', false);
                redirect('inventory/satuan/add');
            }
        }
    }

    public function edit($getId)
    {
        $id = $getId;
        $this->_validasi();

        if ($this->form_validation->run() == false) {
            $data['title'] = "Satuan";
            $data['satuan'] = $this->Inventory_model->get('satuan', ['id_satuan' => $id]);

    		$this->load->view("tamplate/tamplate_inventory/header.php");
            $this->load->view("inventory/edit_satuan.php", $data);  
             $this->load->view("tamplate/tamplate_inventory/footer.php");
        } else {
            $input = $this->input->post(null, true);
            $update = $this->Inventory_model->update('satuan', 'id_satuan', $id, $input);
            if ($update) {
                set_pesan('data berhasil disimpan');
                redirect('inventory/satuan');
            } else {
                set_pesan('data gagal disimpan', false);
                redirect('inventory/satuan/add');
            }
        }
    }

    public function delete($getId)
    {
        $id = $getId;
        if ($this->Inventory_model->delete('satuan', 'id_satuan', $id)) {
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('inventory/satuan');
    }



    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}