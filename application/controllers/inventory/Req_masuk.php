<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Req_masuk extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model Inventory_model
        $this->load->model('Admin',  'admin');
        $this->load->model('Inventory_model');
        $this->load->library('form_validation');
         $this->load->helper(['url','form', 'fungsi_helper']);
        //cek session dan level user
        if($this->Admin->is_role() != "inventory"){
            redirect("login/");
        }
    }

    public function index()
    {
    	   $data['title'] = "Req Barang Masuk";
        $data['req_barangmasuk'] = $this->Inventory_model->getReqBarangMasuk();
            
    $this->load->view("tamplate/tamplate_inventory/header.php");
    $this->load->view("inventory/req_masuk.php", $data);   
     $this->load->view("tamplate/tamplate_inventory/footer.php");
   
            

    }

     private function _validasi()
    {
         $this->form_validation->set_rules('tgl_rbm', 'Tanggal Request Barang Masuk', 'required|trim');
        $this->form_validation->set_rules('barang_id', 'Barang', 'required');
        $this->form_validation->set_rules('jumlah_pesanan', 'Jumlah Pesanan', 'required|trim|numeric|greater_than[0]');
        $this->form_validation->set_rules('statusbm_id', 'Statusbm', 'required');
    }

    public function add()
    {
        $this->_validasi();
        if ($this->form_validation->run() == false) {
            $data['title'] = "Req Barang Masuk";
            $data['barang'] = $this->Inventory_model->get('barang');
             $data['statusbm'] = $this->Inventory_model->get('statusbm');

          

           $this->load->view("tamplate/tamplate_inventory/header.php");
    $this->load->view("inventory/add_req_masuk.php", $data);   
     $this->load->view("tamplate/tamplate_inventory/footer.php");
   
        } else {
            $input = $this->input->post(null, true);
            $insert = $this->Inventory_model->insert('req_barangmasuk', $input);

            if ($insert) {
                set_pesan('data berhasil disimpan.');
                redirect('inventory/req_masuk');
            } else {
                set_pesan('Opps ada kesalahan!');
                redirect('inventory/req_masuk/add');
            }
        }
    }



    public function edit($getId)
    {
        $id = $getId;
        $this->_validasi();

        if ($this->form_validation->run() == false) {
             $data['title'] = "Req Barang Masuk";
           $data['barang'] = $this->Inventory_model->get('barang');
             $data['statusbm'] = $this->Inventory_model->get('statusbm');
            $data['req_barangmasuk'] = $this->Inventory_model->get('req_barangmasuk', ['id_req_barangmasuk' => $id]);
            $this->load->view("tamplate/tamplate_inventory/header.php");
            $this->load->view("inventory/edit_req_masuk.php", $data); 
            $this->load->view("tamplate/tamplate_inventory/footer.php");
        
        } else {
            $input = $this->input->post(null, true);
            $update = $this->Inventory_model->update('req_barangmasuk', 'id_req_barangmasuk', $id, $input);

            if ($update) {
                set_pesan('data berhasil disimpan');
                redirect('inventory/req_masuk');
            } else {
                set_pesan('gagal menyimpan data');
                redirect('inventory/req_masuk/edit/' . $id);
            }
        }
    }

   public function delete($getId)
    {
        $id = $getId;
        if ($this->Inventory_model->delete('req_barangmasuk', 'id_req_barangmasuk', $id)) {
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('inventory/req_masuk');
    }
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}