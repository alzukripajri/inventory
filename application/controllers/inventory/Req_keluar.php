<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Req_keluar extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model Inventory_model
        $this->load->model('Admin',  'admin');
        $this->load->model('Inventory_model');
        $this->load->library('form_validation');
         $this->load->helper(['url','form', 'fungsi_helper']);
        //cek session dan level user
        if($this->Admin->is_role() != "inventory"){
            redirect("login/");
        }
    }

    public function index()
    {
    	   $data['title'] = "Req Barang Keluar";
        $data['req_barangkeluar'] = $this->Inventory_model->getReqBarangKeluar();
            
    $this->load->view("tamplate/tamplate_inventory/header.php");
    $this->load->view("inventory/req_keluar.php", $data);   
     $this->load->view("tamplate/tamplate_inventory/footer.php");
   
            

    }

     private function _validasi()
    {
         $this->form_validation->set_rules('tgl_rbk', 'Tanggal Request Barang Keluar', 'required|trim');
        $this->form_validation->set_rules('barang_id', 'Barang', 'required');
        $this->form_validation->set_rules('jumlah_pesananbk', 'Jumlah Pesanan', 'required|trim|numeric|greater_than[0]');
        $this->form_validation->set_rules('statusbk_id', 'Statusbk', 'required');
    }

    public function add()
    {
        $this->_validasi();
        if ($this->form_validation->run() == false) {
            $data['title'] = "Req Barang Keluar";
            $data['barang'] = $this->Inventory_model->get('barang');
             $data['statusbk'] = $this->Inventory_model->get('statusbk');

          

           $this->load->view("tamplate/tamplate_inventory/header.php");
    $this->load->view("inventory/add_req_keluar.php", $data);   
     $this->load->view("tamplate/tamplate_inventory/footer.php");
   
        } else {
            $input = $this->input->post(null, true);
            $insert = $this->Inventory_model->insert('req_barangkeluar', $input);

            if ($insert) {
                set_pesan('data berhasil disimpan.');
                redirect('inventory/req_keluar');
            } else {
                set_pesan('Opps ada kesalahan!');
                redirect('inventory/req_keluar/add');
            }
        }
    }



    public function edit($getId)
    {
        $id = $getId;
        $this->_validasi();

        if ($this->form_validation->run() == false) {
             $data['title'] = "Req Barang Keluar";
           $data['barang'] = $this->Inventory_model->get('barang');
             $data['statusbk'] = $this->Inventory_model->get('statusbk');
            $data['req_barangkeluar'] = $this->Inventory_model->get('req_barangkeluar', ['id_req_barangkeluar' => $id]);
           
            $this->load->view("tamplate/tamplate_inventory/header.php");
            $this->load->view("inventory/edit_req_keluar.php", $data); 
            $this->load->view("tamplate/tamplate_inventory/footer.php");
        
        } else {
            $input = $this->input->post(null, true);
            $update = $this->Inventory_model->update('req_barangkeluar', 'id_req_barangkeluar', $id, $input);

            if ($update) {
                set_pesan('data berhasil disimpan');
                redirect('inventory/req_keluar');
            } else {
                set_pesan('gagal menyimpan data');
                redirect('inventory/req_keluar/edit/' . $id);
            }
        }
    }

   public function delete($getId)
    {
        $id = $getId;
        if ($this->Inventory_model->delete('req_barangkeluar', 'id_req_barangkeluar', $id)) {
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('inventory/req_keluar');
    }
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}