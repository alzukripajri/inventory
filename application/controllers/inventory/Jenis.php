<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('Admin',);
         $this->load->model('Inventory_model');
        $this->load->library('form_validation');
         $this->load->helper(['url','form', 'fungsi_helper']);
        //cek session dan level user
        if($this->Admin->is_role() != "inventory"){
            redirect("login/");
        }
    }

    public function index()
    {

    $data['title'] = "Jenis";
    $data['jenis'] = $this->Inventory_model->get('jenis');
    $this->load->view("tamplate/tamplate_inventory/header.php");
    $this->load->view("inventory/jenis.php", $data);   
     $this->load->view("tamplate/tamplate_inventory/footer.php");
   
            

    }

     private function _validasi()
    {
        $this->form_validation->set_rules('nama_jenis', 'Nama Jenis', 'required|trim');
    }

    public function add()
    {
        $this->_validasi();

        if ($this->form_validation->run() == false) {
            $data['title'] = "Jenis";
            $this->load->view("inventory/jenis.php", $data);  
        } else {
            $input = $this->input->post(null, true);
            $insert = $this->Inventory_model->insert('jenis', $input);
            if ($insert) {
                set_pesan('data berhasil disimpan');
                redirect('inventory/jenis');
            } else {
                set_pesan('data gagal disimpan', false);
                redirect('inventory/jenis/add');
            }
        }
    }

    public function edit($getId)
    {
        $id = $getId;
        $this->_validasi();

        if ($this->form_validation->run() == false) {
            $data['title'] = "Jenis";
            $data['jenis'] = $this->Inventory_model->get('jenis', ['id_jenis' => $id]);
             $this->load->view("tamplate/tamplate_inventory/header.php");
            $this->load->view("inventory/edit_jenis.php", $data);   
            $this->load->view("tamplate/tamplate_inventory/footer.php");
        } else {
            $input = $this->input->post(null, true);
            $update = $this->Inventory_model->update('jenis', 'id_jenis', $id, $input);
            if ($update) {
                set_pesan('data berhasil disimpan');
                redirect('inventory/jenis');
            } else {
                set_pesan('data gagal disimpan', false);
                redirect('inventory/jenis/add');
            }
        }
    }

    public function delete($getId)
    {
        $id = $getId;
        if ($this->Inventory_model->delete('jenis', 'id_jenis', $id)) {
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('inventory/jenis');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}