<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
          $this->load->model('Inventory_model');
        $this->load->library('form_validation');
         $this->load->helper(['url','form', 'fungsi_helper']);
        //cek session dan level user
        if($this->admin->is_role() != "inventory"){
            redirect("login/");
        }
    }

    public function index()
    {

      $data['title'] = "Barang";
    $data['barang'] = $this->Inventory_model->getBarang();     
    $this->load->view("tamplate/tamplate_inventory/header.php");
    $this->load->view("inventory/barang.php", $data);   
     $this->load->view("tamplate/tamplate_inventory/footer.php");
   
            

    }

     private function _validasi()
    {
        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required|trim');
        $this->form_validation->set_rules('jenis_id', 'Jenis Barang', 'required');
        $this->form_validation->set_rules('satuan_id', 'Satuan Barang', 'required');
    }

    public function add()
    {
        $this->_validasi();

        if ($this->form_validation->run() == false) {
            $data['title'] = "Barang";
            $data['jenis'] = $this->Inventory_model->get('jenis');
            $data['satuan'] = $this->Inventory_model->get('satuan');

            // Mengenerate ID Barang
            $kode_terakhir = $this->Inventory_model->getMax('barang', 'id_barang');
            $kode_tambah = substr($kode_terakhir, -6, 6);
            $kode_tambah++;
            $number = str_pad($kode_tambah, 6, '0', STR_PAD_LEFT);
            $data['id_barang'] = 'B' . $number;
            $this->load->view("tamplate/tamplate_inventory/header.php");
            $this->load->view("inventory/add_stock.php", $data); 
            $this->load->view("tamplate/tamplate_inventory/footer.php");
        } else {
            $input = $this->input->post(null, true);
            $insert = $this->Inventory_model->insert('barang', $input);

            if ($insert) {
                set_pesan('data berhasil disimpan');
                redirect('inventory/barang');
            } else {
                set_pesan('gagal menyimpan data');
                redirect('inventory/barang/add_stock');
            }
        }
    }

    public function edit($getId)
    {
        $id = $getId;
        $this->_validasi();

        if ($this->form_validation->run() == false) {
            $data['title'] = "Barang";
            $data['jenis'] = $this->Inventory_model->get('jenis');
            $data['satuan'] = $this->Inventory_model->get('satuan');
            $data['barang'] = $this->Inventory_model->get('barang', ['id_barang' => $id]);
            $this->load->view("tamplate/tamplate_inventory/header.php");
            $this->load->view("inventory/edit_stock.php", $data); 
            $this->load->view("tamplate/tamplate_inventory/footer.php");
        
        } else {
            $input = $this->input->post(null, true);
            $update = $this->Inventory_model->update('barang', 'id_barang', $id, $input);

            if ($update) {
                set_pesan('data berhasil disimpan');
                redirect('inventory/barang');
            } else {
                set_pesan('gagal menyimpan data');
                redirect('inventory/barang/edit/' . $id);
            }
        }
    }

    public function delete($getId)
    {
        $id = $getId;
        if ($this->Inventory_model->delete('barang', 'id_barang', $id)) {
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('inventory/barang');
    }

    public function getstok($getId)
    {
        $id = $getId;;
        $query = $this->Inventory_model->cekStok($id);
        output_json($query);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}