<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
          $this->load->model('Inventory_model');
        $this->load->library('form_validation');
         $this->load->helper(['url','form', 'fungsi_helper']);
        $this->load->model('admin');
        //cek session dan level user
        if($this->admin->is_role() != "inventory"){
            redirect("login/");
        }
    }

    public function index()
     {
        $data['title'] = "Dashboard";
        $data['barang'] = $this->Inventory_model->count('barang');
        $data['barang_masuk'] = $this->Inventory_model->count('barang_masuk');
        $data['barang_keluar'] = $this->Inventory_model->count('barang_keluar');
        $data['supplier'] = $this->Inventory_model->count('supplier');
        // $data['user'] = $this->Inventory_model->count('user');
        $data['stok'] = $this->Inventory_model->sum('barang', 'stok');
        $data['barang_min'] = $this->Inventory_model->min('barang', 'stok', 10);
        $data['transaksi'] = [
            'barang_masuk' => $this->Inventory_model->getBarangMasuk(5),
            'barang_keluar' => $this->Inventory_model->getBarangKeluar(5)
        ];

        // Line Chart
        $bln = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        $data['cbm'] = [];
        $data['cbk'] = [];

        foreach ($bln as $b) {
            $data['cbm'][] = $this->Inventory_model->chartBarangMasuk($b);
            $data['cbk'][] = $this->Inventory_model->chartBarangKeluar($b);
        }

            
    $this->load->view("tamplate/tamplate_inventory/header.php");
    $this->load->view("inventory/dashboard.php", $data);   
     $this->load->view("tamplate/tamplate_inventory/footer.php");
   
            

    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}