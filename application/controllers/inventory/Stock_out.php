<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_out extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
           $this->load->model('Inventory_model');
        $this->load->library('form_validation');
         $this->load->helper(['url','form', 'fungsi_helper']);
        //cek session dan level user
        if($this->admin->is_role() != "inventory"){
            redirect("login/");
        }
    }

    public function index()
    {

          $data['title'] = "Barang keluar";
        $data['barangkeluar'] = $this->Inventory_model->getBarangkeluar();   
    $this->load->view("tamplate/tamplate_inventory/header.php");
    $this->load->view("inventory/stock_out.php", $data);   
     $this->load->view("tamplate/tamplate_inventory/footer.php");
   
            

    }


     private function _validasi()
    {
        $this->form_validation->set_rules('tanggal_keluar', 'Tanggal Keluar', 'required|trim');
        $this->form_validation->set_rules('barang_id', 'Barang', 'required');

        $input = $this->input->post('barang_id', true);
        $stok = $this->Inventory_model->get('barang', ['id_barang' => $input])['stok'];
        $stok_valid = $stok + 1;

        $this->form_validation->set_rules(
            'jumlah_keluar',
            'Jumlah Keluar',
            "required|trim|numeric|greater_than[0]|less_than[{$stok_valid}]",
            [
                'less_than' => "Jumlah Keluar tidak boleh lebih dari {$stok}"
            ]
        );
    }

    public function add()
    {
        $this->_validasi();
        if ($this->form_validation->run() == false) {
            $data['title'] = "Barang Keluar";
            $data['barang'] = $this->Inventory_model->get('barang', null, ['stok >' => 0]);

            // Mendapatkan dan men-generate kode transaksi barang keluar
            $kode = 'T-BK-' . date('ymd');
            $kode_terakhir = $this->Inventory_model->getMax('barang_keluar', 'id_barang_keluar', $kode);
            $kode_tambah = substr($kode_terakhir, -5, 5);
            $kode_tambah++;
            $number = str_pad($kode_tambah, 5, '0', STR_PAD_LEFT);
            $data['id_barang_keluar'] = $kode . $number;

              $this->load->view("tamplate/tamplate_inventory/header.php");
                $this->load->view("inventory/add_stock_out.php", $data);   
                 $this->load->view("tamplate/tamplate_inventory/footer.php");
        } else {
            $input = $this->input->post(null, true);
            $insert = $this->Inventory_model->insert('barang_keluar', $input);

            if ($insert) {
                set_pesan('data berhasil disimpan.');
                redirect('inventory/stock_out');
            } else {
                set_pesan('Opps ada kesalahan!');
                redirect('inventory/stock_out/add');
            }
        }
    }

    public function delete($getId)
    {
        $id = $getId;
        if ($this->Inventory_model->delete('barang_keluar', 'id_barang_keluar', $id)) {
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('inventory/stock_out');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}