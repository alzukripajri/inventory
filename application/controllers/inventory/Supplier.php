<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model Inventory_model
        $this->load->model('Admin',  'admin');
        $this->load->model('Inventory_model');
        $this->load->library('form_validation');
         $this->load->helper(['url','form', 'fungsi_helper']);
        //cek session dan level user
        if($this->Admin->is_role() != "inventory"){
            redirect("login/");
        }
    }

    public function index()
    {
    	 $data['title'] = "Supplier";
        $data['supplier'] = $this->Inventory_model->get('supplier');
            
    $this->load->view("tamplate/tamplate_inventory/header.php");
    $this->load->view("inventory/supplier.php", $data);   
     $this->load->view("tamplate/tamplate_inventory/footer.php");
   
            

    }

     private function _validasi()
    {
        $this->form_validation->set_rules('nama_supplier', 'Nama Supplier', 'required|trim');
        $this->form_validation->set_rules('no_telp', 'Nomor Telepon', 'required|trim|numeric');
        $this->form_validation->set_rules('no_rek', 'Nomor Rekening', 'required|trim|numeric');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
    }

    public function add()
    {
        $this->_validasi();
        if ($this->form_validation->run() == false) {
            $data['title'] = "Supplier";
           $this->load->view("inventory/supplier.php", $data); 
        } else {
            $input = $this->input->post(null, true);
            $save = $this->Inventory_model->insert('supplier', $input);
            if ($save) {
                set_pesan('data berhasil disimpan.');
                redirect('inventory/supplier');
            } else {
                set_pesan('data gagal disimpan', false);
                redirect('inventory/supplier/add');
            }
        }
    }


    public function edit($getId)
    {
        $id = $getId;
        $this->_validasi();

        if ($this->form_validation->run() == false) {
            $data['title'] = "Supplier";
            $data['supplier'] = $this->Inventory_model->get('supplier', ['id_supplier' => $id]);
                $this->load->view("tamplate/tamplate_inventory/header.php");
   				 $this->load->view("inventory/edit_supplier.php", $data);   
    			 $this->load->view("tamplate/tamplate_inventory/footer.php");
        } else {
            $input = $this->input->post(null, true);
            $update = $this->Inventory_model->update('supplier', 'id_supplier', $id, $input);

            if ($update) {
                set_pesan('data berhasil diedit.');
                redirect('inventory/supplier');
            } else {
                set_pesan('data gagal diedit.');
                redirect('inventory/supplier/edit/' . $id);
            }
        }
    }

    public function delete($getId)
    {
        $id = $getId;
        if ($this->Inventory_model->delete('supplier', 'id_supplier', $id)) {
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('inventory/supplier');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}