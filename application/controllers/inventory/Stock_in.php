<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_in extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load model admin
        $this->load->model('admin');
           $this->load->model('Inventory_model');
        $this->load->library('form_validation');
         $this->load->helper(['url','form', 'fungsi_helper']);
        //cek session dan level user
        if($this->admin->is_role() != "inventory"){
            redirect("login/");
        }
    }

    public function index()
    {
         $data['title'] = "Barang Masuk";
        $data['barangmasuk'] = $this->Inventory_model->getBarangMasuk();
            
    $this->load->view("tamplate/tamplate_inventory/header.php");
    $this->load->view("inventory/stock_in.php", $data);   
     $this->load->view("tamplate/tamplate_inventory/footer.php");
   
            

    }

      private function _validasi()
    {
        $this->form_validation->set_rules('tanggal_masuk', 'Tanggal Masuk', 'required|trim');
        $this->form_validation->set_rules('supplier_id', 'Supplier', 'required');
        $this->form_validation->set_rules('barang_id', 'Barang', 'required');
        $this->form_validation->set_rules('jumlah_masuk', 'Jumlah Masuk', 'required|trim|numeric|greater_than[0]');
    }

    public function add()
    {
        $this->_validasi();
        if ($this->form_validation->run() == false) {
            $data['title'] = "Barang Masuk";
            $data['supplier'] = $this->Inventory_model->get('supplier');
            $data['barang'] = $this->Inventory_model->get('barang');

            // Mendapatkan dan men-generate kode transaksi barang masuk
            $kode = 'T-BM-' . date('ymd');
            $kode_terakhir = $this->Inventory_model->getMax('barang_masuk', 'id_barang_masuk', $kode);
            $kode_tambah = substr($kode_terakhir, -5, 5);
            $kode_tambah++;
            $number = str_pad($kode_tambah, 5, '0', STR_PAD_LEFT);
            $data['id_barang_masuk'] = $kode . $number;

           $this->load->view("tamplate/tamplate_inventory/header.php");
    $this->load->view("inventory/add_stock_in.php", $data);   
     $this->load->view("tamplate/tamplate_inventory/footer.php");
   
        } else {
            $input = $this->input->post(null, true);
            $insert = $this->Inventory_model->insert('barang_masuk', $input);

            if ($insert) {
                set_pesan('data berhasil disimpan.');
                redirect('inventory/stock_in');
            } else {
                set_pesan('Opps ada kesalahan!');
                redirect('inventory/stock_in/add');
            }
        }
    }

    public function delete($getId)
    {
        $id = $getId;
        if ($this->Inventory_model->delete('barang_masuk', 'id_barang_masuk', $id)) {
            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }
        redirect('inventory/stock_in');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}